/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package agregacionbombas;

import javax.swing.JDesktopPane;
import javax.swing.JOptionPane;
import javax.swing.JFrame;
import javax.swing.JTextField;

/**
 *
 * @author sebas
 */
public class jintGasolina extends javax.swing.JInternalFrame {

    /**
     * Creates new form jintGasolina
     */
    public jintGasolina() {
        initComponents();
        setSize(950,890);
        this.deshabilitar();
    }

    public void deshabilitar(){
        this.txtNumBomba.setEnabled(false);
        this.cmbTipo.setEnabled(false);
        this.txtContador.setEnabled(false);
        this.txtPrecio.setEnabled(false);
        this.btmHacerVenta.setEnabled(false);
        this.jslCapacidad.setEnabled(false);
        this.txtCantidad.setEnabled(false);
        this.txtTotalVenta.setEnabled(false); 
    }
    
    public void habilitar(){
        this.txtNumBomba.setEnabled(!false);
        this.cmbTipo.setEnabled(!false);
        this.txtContador.setEnabled(!false);
        this.txtPrecio.setEnabled(!false);
        this.btmHacerVenta.setEnabled(!false);
        this.jslCapacidad.setEnabled(!false);
    }
    
    public void limpiar(){
        this.txtNumBomba.setText("");
        this.cmbTipo.setSelectedIndex(0);
        this.txtContador.setText("");
        this.txtPrecio.setText("");
        this.jslCapacidad.setValue(this.jslCapacidad.getMinimum());  // Resetea el jslider a su valor minimo
        this.txtCantidad.setText("");
        this.txtTotalVenta.setText(""); 
    }
    
    
    
    @SuppressWarnings("unchecked")
    
    
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        cmbTipo = new javax.swing.JComboBox<>();
        txtContador = new javax.swing.JTextField();
        txtPrecio = new javax.swing.JTextField();
        txtNumBomba = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        jslCapacidad = new javax.swing.JSlider();
        btmInicia = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        jLabel7 = new javax.swing.JLabel();
        txtCantidad = new javax.swing.JTextField();
        btmHacerVenta = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        txtTotalVenta = new javax.swing.JTextField();
        btmCerrar = new javax.swing.JButton();
        btmLimpiar = new javax.swing.JButton();

        setBackground(new java.awt.Color(204, 204, 204));
        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        getContentPane().setLayout(null);

        jPanel1.setBackground(new java.awt.Color(51, 204, 0));
        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "PEMEX", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Verdana", 2, 13), new java.awt.Color(255, 255, 255))); // NOI18N
        jPanel1.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jPanel1.setLayout(null);

        jLabel2.setFont(new java.awt.Font("Verdana", 2, 14)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setText("Num.Bomba");
        jPanel1.add(jLabel2);
        jLabel2.setBounds(20, 70, 100, 18);

        jLabel3.setFont(new java.awt.Font("Verdana", 2, 14)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(255, 255, 255));
        jLabel3.setText("Tipo de Gasolina");
        jPanel1.add(jLabel3);
        jLabel3.setBounds(200, 70, 120, 18);

        jLabel4.setFont(new java.awt.Font("Verdana", 2, 14)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(255, 255, 255));
        jLabel4.setText("Precio de Venta");
        jPanel1.add(jLabel4);
        jLabel4.setBounds(410, 140, 111, 18);

        cmbTipo.setFont(new java.awt.Font("Verdana", 2, 13)); // NOI18N
        cmbTipo.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Regular", "Premium", " " }));
        jPanel1.add(cmbTipo);
        cmbTipo.setBounds(200, 100, 90, 23);

        txtContador.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txtContador.setText("0");
        txtContador.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtContadorActionPerformed(evt);
            }
        });
        jPanel1.add(txtContador);
        txtContador.setBounds(550, 70, 110, 22);

        txtPrecio.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txtPrecio.setText("0");
        txtPrecio.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtPrecioActionPerformed(evt);
            }
        });
        jPanel1.add(txtPrecio);
        txtPrecio.setBounds(530, 140, 120, 22);

        txtNumBomba.setText("0");
        jPanel1.add(txtNumBomba);
        txtNumBomba.setBounds(20, 90, 80, 22);

        jLabel5.setFont(new java.awt.Font("Verdana", 2, 14)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(255, 255, 255));
        jLabel5.setText("Contador de litros");
        jPanel1.add(jLabel5);
        jLabel5.setBounds(410, 70, 130, 18);

        jslCapacidad.setFont(new java.awt.Font("Verdana", 2, 13)); // NOI18N
        jslCapacidad.setMajorTickSpacing(10);
        jslCapacidad.setPaintLabels(true);
        jslCapacidad.setPaintTicks(true);
        jslCapacidad.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                jslCapacidadStateChanged(evt);
            }
        });
        jPanel1.add(jslCapacidad);
        jslCapacidad.setBounds(40, 210, 780, 60);

        btmInicia.setBackground(new java.awt.Color(255, 51, 51));
        btmInicia.setFont(new java.awt.Font("Verdana", 2, 13)); // NOI18N
        btmInicia.setText("INICIAR BOMBA");
        btmInicia.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btmIniciaActionPerformed(evt);
            }
        });
        jPanel1.add(btmInicia);
        btmInicia.setBounds(670, 70, 150, 70);

        getContentPane().add(jPanel1);
        jPanel1.setBounds(10, 20, 890, 310);

        jPanel2.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel2.setLayout(null);

        jLabel7.setFont(new java.awt.Font("Verdana", 2, 14)); // NOI18N
        jLabel7.setText("Cantidad");
        jPanel2.add(jLabel7);
        jLabel7.setBounds(70, 40, 70, 18);

        txtCantidad.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txtCantidad.setText("0");
        txtCantidad.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtCantidadActionPerformed(evt);
            }
        });
        jPanel2.add(txtCantidad);
        txtCantidad.setBounds(140, 40, 140, 20);

        btmHacerVenta.setFont(new java.awt.Font("Verdana", 2, 14)); // NOI18N
        btmHacerVenta.setText("Hacer la venta");
        btmHacerVenta.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btmHacerVentaActionPerformed(evt);
            }
        });
        jPanel2.add(btmHacerVenta);
        btmHacerVenta.setBounds(430, 30, 150, 40);

        getContentPane().add(jPanel2);
        jPanel2.setBounds(20, 400, 870, 190);

        jLabel1.setFont(new java.awt.Font("Verdana", 2, 24)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 51, 0));
        jLabel1.setText("Venta de Gasolina");
        getContentPane().add(jLabel1);
        jLabel1.setBounds(340, 360, 220, 30);

        jLabel6.setFont(new java.awt.Font("Verdana", 2, 14)); // NOI18N
        jLabel6.setText("Total venta $");
        getContentPane().add(jLabel6);
        jLabel6.setBounds(520, 610, 100, 18);

        txtTotalVenta.setText("0");
        txtTotalVenta.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtTotalVentaActionPerformed(evt);
            }
        });
        getContentPane().add(txtTotalVenta);
        txtTotalVenta.setBounds(620, 610, 100, 22);

        btmCerrar.setFont(new java.awt.Font("Verdana", 2, 14)); // NOI18N
        btmCerrar.setText("Cerrar");
        btmCerrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btmCerrarActionPerformed(evt);
            }
        });
        getContentPane().add(btmCerrar);
        btmCerrar.setBounds(20, 630, 110, 30);

        btmLimpiar.setFont(new java.awt.Font("Verdana", 2, 14)); // NOI18N
        btmLimpiar.setText("Limpiar");
        btmLimpiar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btmLimpiarActionPerformed(evt);
            }
        });
        getContentPane().add(btmLimpiar);
        btmLimpiar.setBounds(150, 630, 100, 30);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txtPrecioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtPrecioActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtPrecioActionPerformed

    private void btmIniciaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btmIniciaActionPerformed
        // TODO add your handling code here:
        bombas = new Bomba();
        this.habilitar();
        
        bombas = new Bomba(1, 1000.0f, 0.0f, new Gasolina(1, "Pemex", 1, 20.0f));
        this.habilitar();
          
        
    }//GEN-LAST:event_btmIniciaActionPerformed

    private void txtTotalVentaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtTotalVentaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtTotalVentaActionPerformed

    private void btmCerrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btmCerrarActionPerformed
        // TODO add your handling code here:
        int opcion = 0;
        opcion = JOptionPane.showConfirmDialog(this, "Deseas salir",
            "Pago Gasolina", JOptionPane.YES_NO_OPTION);
        if (opcion == JOptionPane.YES_OPTION){
            this.dispose();

        }
    }//GEN-LAST:event_btmCerrarActionPerformed

    private void btmLimpiarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btmLimpiarActionPerformed
        // TODO add your handling code here:
        this.txtNumBomba.setText("");
        this.cmbTipo.setSelectedIndex(0);
        this.txtContador.setText("");
        this.txtPrecio.setText("");
        this.jslCapacidad.setValue(this.jslCapacidad.getMinimum());  // Resetear el JSlider a su valor mínimo
        this.txtCantidad.setText("");
        this.txtTotalVenta.setText(""); 
    }//GEN-LAST:event_btmLimpiarActionPerformed

    private void btmHacerVentaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btmHacerVentaActionPerformed
      
      float cantidad = (float) jslCapacidad.getValue();

    if (bombas.realizarVenta(cantidad)) {
        // Calculate the total price of the sale
        float totalPagar = bombas.calcularTotal();

        // Update the text fields with the new values
        this.txtContador.setText(String.valueOf(bombas.getContador()));
        this.txtPrecio.setText(String.valueOf(bombas.getGasolinera().getPrecio()));
        this.txtTotalVenta.setText(String.valueOf(totalPagar));
    } else {
        JOptionPane.showMessageDialog(this, "No hay suficiente gasolina en la bomba");
    }
    }//GEN-LAST:event_btmHacerVentaActionPerformed

    private void jslCapacidadStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_jslCapacidadStateChanged
        // TODO add your handling code here:
        this.txtCantidad.setText("la cantidad es" + jslCapacidad.getValue());
        
    }//GEN-LAST:event_jslCapacidadStateChanged

    
    private void txtContadorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtContadorActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtContadorActionPerformed

    private void txtCantidadActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtCantidadActionPerformed
        // TODO add your handling code here:
        
    }//GEN-LAST:event_txtCantidadActionPerformed

    public static void main(String[] args) {
        JFrame frame = new JFrame();
        JDesktopPane desktop = new JDesktopPane();
        frame.add(desktop);

        jintGasolina internalFrame = new jintGasolina();
        internalFrame.setTitle("Gasolinera");
        desktop.add(internalFrame);
        internalFrame.setVisible(true);
        internalFrame.setSize(850, 850);
        internalFrame.setLocation(50, 50);

        frame.setSize(1000, 850);
        frame.setVisible(true);
    }
   

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btmCerrar;
    private javax.swing.JButton btmHacerVenta;
    private javax.swing.JButton btmInicia;
    private javax.swing.JButton btmLimpiar;
    private javax.swing.JComboBox<String> cmbTipo;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JSlider jslCapacidad;
    private javax.swing.JTextField txtCantidad;
    private javax.swing.JTextField txtContador;
    private javax.swing.JTextField txtNumBomba;
    private javax.swing.JTextField txtPrecio;
    private javax.swing.JTextField txtTotalVenta;
    // End of variables declaration//GEN-END:variables
    private Bomba bombas = new Bomba();
    private Gasolina gas = new Gasolina();

}
