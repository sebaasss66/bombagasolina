/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package agregacionbombas;

/**
 *
 * @author sebas
 */
public class Gasolina {
    
    int id;
    String marca;
    int tipo;
    float precio;
 
    public Gasolina(int gasolinera, String marca, int tipo, float precio) {
        this.id = gasolinera;
        this.marca = marca;
        this.tipo = tipo;
        this.precio = precio;
    }

    public Gasolina() {
        
        this.id = 0;
        this.marca = "";
        this.tipo = 0;
        this.precio = 0.0f;
    }
    
    public Gasolina(Gasolina gasolinera) {
        
        this.id = gasolinera.id;
        this.marca = gasolinera.marca;
        this.tipo = gasolinera.tipo;
        this.precio = gasolinera.precio;
    }
    
    public int getGasolinera() {
        return id;
    }

    public void setGasolinera(int gasolinera) {
        this.id = gasolinera;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public int getTipo() {
        return tipo;
    }

    public void setTipo(int tipo) {
        this.tipo = tipo;
    }

    public float getPrecio() {
        return precio;
    }

    public void setPrecio(float precio) {
        this.precio = precio;
    }
      
    public String mostrarInformacion(){
        String info = "";
        info = "id" + this.id + "marca" + this.marca + "tipo" + this.tipo + "precio" + this.precio;
         return "id: " + this.id + ", marca: " + this.marca + ", tipo: " + this.tipo + ", precio: " + this.precio; 
     
    }
    
    
    
}
